﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The core project namespace containing all code needed to build the project to a device and including *no* code not essential to doing so.
/// </summary>
namespace BBB
{
    public static class Utilities
    {
        /// <summary>
        /// Divides a name up using dashes, and trims the resulting strings
        /// </summary>
        /// <param name="name">A string containing dashes</param>
        /// <returns>array of trimmed strings</returns>
        public static string[] SplitName(string name)
        {
            List<string> split = new List<string>(name.Split('-'));
            List<string> trimmed = new List<string>();
            foreach (string str in split)
            {
                trimmed.Add(str.Trim());
            }
            return trimmed.ToArray();
        }

        /// <summary>
        /// meta method for the public, 2 string version. Allows us to expand the naming convention in the future if we need to
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private static string JoinName(IEnumerable<string> list)
        {
            List<string> trimmed = new List<string>();
            foreach (string s in list)
            {
                trimmed.Add(s.Trim());
            }
            string name = String.Join(" - ", trimmed);
            return name;
        }

        /// <summary>
        /// concatenates names with space-dash-space separators
        /// </summary>
        /// <param name="list">the strings to join</param>
        /// <returns>a single string separated with space-dash-space.</returns>
        public static string JoinName(string style, string variant)
        {
            if (style == null || style == "" || variant == null || variant  == "")
            {
                throw new Exception("This version of Join Name cannot work when one argument is null.");
            }
            string name = JoinName(new string[] { style, variant });
            return name;
        }

        /// <summary>
        /// Apple garbage. replaces likely special characters with underscores
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Pathify(this string str)
        {
            return str.ToLower()
                .Replace(" ", "_")
                .Replace("&", "_")
                .Replace("'", "_")
                .Replace("-", "_");
        }

        public static string CsvWrap(this string str) {
            if(str.Contains(",")) {
                return "\"" + str + "\"";
            }
            else {
                return str;
            }
        }
        public static string CsvScrub(this string str) {
            return str.Replace(",", "_");
        }

        /// <summary>
        /// Apple garbage. makes a string safe for inclusion in the transporter `*.itmsp` `metadata.xml` file
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string XmlSafe(this string str)
        {
            return System.Web.HttpUtility.HtmlEncode(str);
        }

        /// <summary>
        /// Apple garbage. apple imposes an abritrary and obnoxious character limit on the title of IAP items.
        /// This shortens a string to fit.
        /// </summary>
        /// <returns></returns>
        public static string AppleCrunch(this string str)
        {
            // Magic number provided by apple.
            // See https://gitlab.com/bridal-builder/bbb4/-/issues/223#note_556427344
            int maxChars = 30;
            // abbreviate each word
            string cleaned = str.RemoveSpecialCharacters();
            string[] words = cleaned.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            StringBuilder sb = new StringBuilder();
            foreach (string word in words)
            {
                if (word.Length > 3)
                {
                    string abbr = word.Substring(1, 3);
                    string a = word[0].ToString().ToUpper();
                    sb.Append(a + abbr);
                }
                else
                {
                    string abbr = word.Substring(1);
                    string a = word[0].ToString().ToUpper();
                    sb.Append(a + abbr);
                }
            }
            // cull BEGINNING of string down to maxChars
            // the beginning is most likely to be non-unique if we have overflow
            string fullCompressedString = sb.ToString();
            if (fullCompressedString.Length > maxChars)
            {
                Debug.LogWarning($"{str} name was too long even with compression and had to be culled." +
                    $"nothing needs to be done, but it may be hard to read in apple's iap list");
                return fullCompressedString.Substring(fullCompressedString.Length - maxChars);
            }
            else
            {
                return fullCompressedString;
            }
        }

        /// <summary>
        /// supports AppleCrunch()
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RemoveSpecialCharacters(this string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Booleans the bounds an object with an arbitrarily deep hierarchy.
        /// </summary>
        /// <param name="gameObject">the object or prefab to get the total bounds of</param>
        /// <param name="bounds">pass no bounds in on starting object.</param>
        /// <returns></returns>
        public static Bounds UnionBounds(GameObject gameObject, Bounds bounds = new Bounds())
        {
            // do this object
            //MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
            Renderer renderer = gameObject.GetComponent<Renderer>();
            if (renderer != null)
            {
                bounds.Encapsulate(renderer.bounds);
            }
            // do children objects
            foreach (Transform child in gameObject.transform)
            {
                bounds.Encapsulate(UnionBounds(child.gameObject));
            }
            // pass it on
            return bounds;
        }

        /// <summary>
        /// Writes a Texture2D to disk as a PNG
        /// </summary>
        /// <param name="texture2D"></param>
        /// <param name="guid"></param>
        public static string SaveTextureLocalPng(Texture2D texture2D, string guid)
        {
            string path = Application.persistentDataPath + System.IO.Path.DirectorySeparatorChar + guid + ".png";
            byte[] bytes = texture2D.EncodeToPNG();
            System.IO.File.WriteAllBytes(path, bytes);
            Debug.Log(bytes.Length / 1024 + "Kb was saved as: " + path);
            return path;
        }

        /// <summary>
        /// Sets the layer of an arbitrarily deep hierarchy to 9, the thumbnail layer
        /// </summary>
        /// <param name="transform">the transform of the object to set the layer on</param>
        public static void SetLayerRecursively(Transform transform, int layer)
        {
            transform.gameObject.layer = layer;
            if (transform.childCount > 0)
            {
                foreach (Transform child in transform)
                {
                    SetLayerRecursively(child, layer);
                }
            }
            else
            {
                // no children, all done in this recursion
            }
        }

        /// <summary>
        /// Replaces the material on every renderer in a given transform
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="material"></param>
        public static void ReplaceMaterialRecursively(Transform transform, Material material)
        {
            Renderer renderer = transform.gameObject.GetComponent<Renderer>();
            if (renderer != null)
            {
                renderer.material = material;
            }
            else
            {
                // no renderer found, no big deal
            }
            if (transform.childCount > 0)
            {
                foreach (Transform child in transform)
                {
                    ReplaceMaterialRecursively(child, material);
                }
            }
            else
            {
                // no children, all done in this recursion
            }
        }

        /// <summary>
        /// Converts a single Float in inches to Meters
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        public static float InchesToMeters (float f)
        {
            return f / 39.3701f;
        }

        /// <summary>
        /// Converts a Vector3 in Inches to Meters
        /// </summary>
        /// <param name="v">A Vector3 in Inches</param>
        /// <returns></returns>
        public static Vector3 InchesToMeters (Vector3 v)
        {
            return v / 39.3701f;
        }

        public static string Print(this List<string> list)
        {
            string o = string.Join("\n", list);
            return o;
        }
        public static string Print(this string[] list)
        {
            string o = string.Join("\n", list);
            return o;
        }

        /// <summary>
        /// Unions two flag enums and returns whether they're overlapping
        /// </summary>
        /// <remarks>
        /// I don't use bitwise operations often, so I'm making a note of how this union operation should work.
        /// 
        /// `a` is the first instance. `b` is the second. aN is the Nth bit of the enum.
        /// 
        /// ```
        /// a1 & b1 = c1
        /// a2 & b2 = c2
        /// a3 & b3 = c3
        /// a4 & b4 = c4
        /// ..   ..   ..
        /// aN & bN = cN
        /// ```
        /// 
        /// once we've created instance `c`, we can cast it to an Int.
        /// Because of how binary numbers work, any flag being set means the Int will be greater than 0,
        /// so we just check to see if the int is greater than 0 to know if there was a successful union between `a` and `b`
        /// 
        /// ```
        /// (int) c > 0 = true
        /// (int) c = 0 = false
        /// ```
        /// </remarks>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>true if there's overlap, false otherwise.</returns>
        // public static bool DoFlagsOverlap(Model.ShapeTypes a, Model.ShapeTypes b)
        // {
        //     Model.ShapeTypes c = a & b;
        //     return (int)c > 0 ? true : false;
        // }

        /// <summary>
        /// prints an entire list or array ond debug.log.
        /// </summary>
        /// <param name="arr"></param>
        public static void DebugLog (this IEnumerable<string> arr)
        {
            UnityEngine.Debug.Log(string.Join("\n", arr));
        }

        /// <summary>
        /// A fast check for whether a list or array of strings contains a specific value
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static bool ContainsString(this IEnumerable<string> arr, string target) {
            foreach(string check in arr){
                if (check == target) {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Gets the position to scroll to in order to bring a child object into view on a scrollview
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="child"></param>
        /// <returns></returns>
        public static Vector2 GetPosOfChildForScroll(this ScrollRect instance, RectTransform child) {
            Canvas.ForceUpdateCanvases();
            Vector2 viewportLocalPosition = instance.viewport.localPosition;
            // Debug.Log(viewportLocalPosition);
            Vector2 childLocalPosition = child.localPosition;
            // Debug.Log(childLocalPosition);
            float x = instance.horizontal ? 0 - (viewportLocalPosition.x + childLocalPosition.x) : 0;
            float y = instance.vertical ? 0 - (viewportLocalPosition.y + childLocalPosition.y) : 0;
            Vector2 result = new Vector2(x,y);
            // Debug.Log(result);
            return result;
        }
        public static Vector2 GetPosOfChildForScroll(this ScrollRect instance, Transform child) {
            RectTransform rt = child.GetComponent<RectTransform>();
            return (instance.GetPosOfChildForScroll(rt));
        }
    }
}