﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BBB.View
{
    /// <summary>
    /// A View utility class for generating thumbnails from 3d objects
    /// </summary>
    public class Thumbnail : MonoBehaviour
    {
        private static Queue<QueuedOrder> queue = new Queue<QueuedOrder>();
        private static bool isRunning = false;
        private static Thumbnail instance;
        private static bool isReady = false;

        private static List<Texture2D> generatedTextures = new List<Texture2D>();

        private static QueuedOrder order = null;
        // private static IEnumerator coroutine;
        private static int renders = 0;


        static public Thumbnail Instance {
            get { 
                if(instance == null) {
                    // Debug.Log("thumbnail instance was null, creating new instance");
                    GameObject go = new GameObject("Thumbnailer");
                    instance = go.AddComponent<Thumbnail>();
                    // instance.Awake();
                    // instance.Start();
                }    
                return instance;
            }
        }

        public void Awake()
        {
            // Debug.Log("thumbnail awake");
            if(instance == null) {
                instance = this;
            }
            else {
                Debug.Log("Awoke Second, I will perform self die");
                Destroy(this.gameObject);
            }
        }

        public void Start()
        {
            Debug.Log("thumbnail start");
            isReady = true;
            if (!isRunning && queue.Count > 0)
            {
                // coroutine = DequeueThumbnail();
                Instance.StartCoroutine("DequeueThumbnail");
            }
            //Debug.Log("thumbnail started");
        }

        private void OnDestroy() {
            if(order != null) {
                Debug.LogError("destroying Thumbnail instance while there's still an order.\nthe render will not finish!");
            }
            if(queue.Count > 0) {
                Debug.LogWarning("destroying Thumbnail instance while there's still a queue.\nthe renders will not finish.");
            }
            isRunning = false;
            isReady = false;
            order = null;
        }

        public static void EnqueueThumbnail(GameObject subject, Image target, string source)
        {
            Debug.Log("thumbnail Enqueue " + source);

            queue.Enqueue(new QueuedOrder(subject, target, source));
            //Debug.Log(queue.Count);
            Debug.Log($"running: {isRunning}, isReady {isReady}");
            if (!isRunning && isReady)
            {
                Instance.StartCoroutine("DequeueThumbnail");
            }
        }

        private IEnumerator DequeueThumbnail()
        {
            Debug.Log("thumbnail Dequeue");
            if (queue.Count > 0)
            {
                isRunning = true;
                order = queue.Dequeue();
                // Debug.Log(order.ToString());
                if(order.gameObject == null) {
                    if(order.image == null) {
                        yield break;
                    }
                    else {
                        Debug.LogError("Thumbnail target object destroyed, but target image still exists.\nIn other words, the thing to be rendered was destroyed before the render happened.");
                    }
                }
                // Debug.Log("Dequeue Thumbnail for " + order.name);
                Texture2D texture2D = GenerateSingle(order.gameObject, Color.clear);
                Sprite sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), Vector2.zero);
                // Debug.LogWarning("image: " + order.image + " " + order.source);
                yield return null;
                // Debug.LogWarning("image: " + order.image + " " + order.source);
                if(sprite == null) {
                    Debug.LogError("sprite was destroyed before being assigned " + order.name + ", " + order.source);
                }
                if(order == null ) {
                    Debug.LogError("order was destroyed in the middle of the coroutine.");
                }
                if(order.image != null) {
                    order.image.sprite = sprite;
                    // Debug.Log("complete: " + order.ToString());
                    order = null;
                    if(queue.Count > 0) {
                        instance.StartCoroutine("DequeueThumbnail");
                        isRunning = true;
                    }
                    else {
                        isRunning = false;
                    }
                }
                else {
                    Debug.LogWarning("order image was destroyed before being assigned to " + order.name + ", " + order.source);
                }
                if(queue.Count == 0) {
                    isRunning = false;
                } 
            }
            else
            {
                Debug.LogWarning("warn: DequeueThumbnail called when queue was empty");
                isRunning = false;
            }
        }

        /// <summary>
        /// generates a transparent thumbnail of the object. 
        /// </summary>
        /// <param name="gameObject">the gameobject or prefab you want to make a thumbnail of</param>
        /// <param name="background">the color to use in the background of the image.</param>
        /// <param name="distance">the distance of of the camera from the object. Defaults to 1.5</param>
        /// <param name="angle">the angle above horizontal to take the shot from</param>
        /// <returns>Returns a Texture2D of the thumbnail</returns>
        private static Texture2D GenerateSingle(GameObject gameObject, Color background, float distance = 1.5f, float angle = 60f)
        {
            // Debug.Log("generate single: " + gameObject.name);
            string name = gameObject.name;
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            if(gameObject == null)
            {
                throw new System.NullReferenceException("You must provide this method an object.");
            }
            Camera camera = Setup();
            camera.backgroundColor = background;
            GameObject root = camera.transform.parent.gameObject;
            //RenderTexture originalTexture = RenderTexture.active;
            RenderTexture.active = camera.targetTexture;
            //RenderTexture texture = RenderTexture.active;

            // instantiate prefab if needed, otherwise use existing
            GameObject instance;
            if (gameObject.scene.name == null)
            {
                instance = GameObject.Instantiate(gameObject, root.transform, false);
            }
            else
            {
                instance = gameObject;
            }
            SlotBouquet slotBouquet = instance.GetComponent<SlotBouquet>();
            if (slotBouquet != null)
            {
                slotBouquet.Setup();
                slotBouquet.ReplaceInsertionSlots();
            }
            //Debug.Log(instance);
            Utilities.SetLayerRecursively(instance.transform, 9);

            // move object to center of camera focus
            Bounds bounds = Utilities.UnionBounds(instance);
            Vector3 meshSize = bounds.size;
            Vector3 center = bounds.center;
            instance.transform.position += center * -1;

            // move camera to frame object
            float zdistance = Mathf.Max(meshSize.x, meshSize.y, meshSize.z);
            zdistance /= (2.0f * Mathf.Tan(0.5f * camera.fieldOfView * Mathf.Deg2Rad));
            camera.transform.position = new Vector3(0, 0, -zdistance * distance);
            camera.transform.RotateAround(Vector3.zero, Vector3.left, angle * -1);
            // process image
            camera.Render();
            int w = camera.targetTexture.width;
            int h = camera.targetTexture.height;
            // Debug.Log($"{w} x {h}");
            Texture2D texture2D = new Texture2D(w, h);
            // Debug.Log(texture2D.isReadable);
            // Debug.Log(texture2D.width + " " + texture2D.height);
            texture2D.ReadPixels(new Rect(0, 0, texture2D.width, texture2D.height), 0, 0);
            texture2D.Apply();
            camera.targetTexture.DiscardContents(true, true);
            if(camera.targetTexture != null) {
                camera.targetTexture.Release();
            }
            if(RenderTexture.active != null) {
                RenderTexture.active.Release();
            }
            // RenderTexture.active.DiscardContents(true, true);
            // RenderTexture removable = RenderTexture.active;
            // RenderTexture.active = null;
            // removable.Release();
            //RenderTexture.active = texture as RenderTexture;
            instance.transform.position = new Vector3(1000, 1000, -1000);
            //Debug.Log(instance.transform.position + " vs " + camera.transform.position);
            Utilities.SetLayerRecursively(instance.transform, 10);
            Teardown(camera);

            // hide everything in thumbnail layer just in case we do another thumbnail immediately. I suspect that Destroy only finishes firing once per step or frame for performance reasons, and that's why multiple thumbnails in the same frame get jumbled together.
            if (instance != null)
            {
                Utilities.SetLayerRecursively(instance.transform, 10);
#if UNITY_EDITOR
                if (Application.isPlaying)
                {
                    GameObject.Destroy(instance);

                }
                else
                {
                    GameObject.DestroyImmediate(instance);
                }

#else
                GameObject.Destroy(instance);

#endif
            }

            // timing
            // Debug.Log("rendered in " + stopwatch.Elapsed.Milliseconds + " ms");
            // stopwatch.Stop();
            stopwatch.Restart();
            texture2D = CropTransparent(texture2D, name);
            // Debug.Log("crop took " + stopwatch.Elapsed.Milliseconds + " ms");
            //Debug.Log("Thumbnail generation time: " + ts.Milliseconds + "ms. Texture: " + texture2D);
            generatedTextures.Add(texture2D);
            Debug.Log("renders: " + ++renders);
            return texture2D;
        }

        /// <summary>
        /// Attemps to return a texture by using the private GenerateSingle, but only if we're in the editor.
        /// If we're in runtime, it throws an exception, because we want runtime to always have to use the queue system, ie EnqueueThumnail()
        /// This method gives us a way to use GenerateSingle without using Reflection in tests and editor scripts.
        /// </summary>
        /// <param name="gameObject">the gameobject or prefab you want to make a thumbnail of</param>
        /// <param name="background"></param>
        /// <param name="distance"></param>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static Texture2D EditorOnlyGenerateImmediately(GameObject gameObject, Color background, float distance = 1.5f, float angle = 60f) {
            if(Application.isEditor) {
                return GenerateSingle(gameObject, background, distance, angle);
            }
            else {
                string err = "EditorOnlyGenerateImmediately is NOT FOR RUNTIME!";
                Debug.LogError(err);
                return Texture2D.redTexture;
                throw new Exception(err);
            }
        }

        public static Texture2D EditorOnlyGenerateImmediately(GameObject gameObject, float distance = 1.5f, float angle = 60f)
        {
            return EditorOnlyGenerateImmediately(gameObject, Color.clear, distance, angle);
        }

        public static Texture2D CropTransparent(Texture2D initial, string name){
            int skip = 5;
            int x = 0;
            int y = 0;
            int w = initial.width;
            int h = initial.height;
            // now to do the cropping algo
            Color[] originalPixels = initial.GetPixels(x, y, w, h);
            int row = 0;
            int col = 0;
            int firstX = w;
            int lastX = 0;
            int firstY = h;
            int lastY = 0;
            for(int i = 0; i < originalPixels.Length; i += skip) {
                row = Mathf.FloorToInt(i / initial.width);
                col = i % w;
                Color pixel = originalPixels[i];
                bool hasColor = pixel != Color.clear;
                if(hasColor) {
                    if(col < firstX) {
                        firstX = col;
                    }
                    if(row < firstY) {
                        firstY = row;
                    }
                    if(col > lastX) {
                        lastX = col;
                    }
                    if(row > lastY) {
                        lastY = row;
                    }
                }
                if(col < skip) {
                    i += w * skip;
                }
            }
            x = firstX;
            y = firstY;
            w = lastX - firstX;
            h = lastY - firstY;
            // Debug.Log("x: " + x + ", y:" + y + ", w:" + w + ", h:" + h);
            // Debug.Log($"lastX {lastX}, lastY {lastY}");
            // sample the cropped frame
            if(w == -1024 || h == -1024) {
                throw new Exception($"Generated image of {name} was blank! Check to make sure it has a mesh and renderer.");
            }
            // Debug.Log(x + " " + y + " : " + w + " " + h);
            Color[] sampledPixels = initial.GetPixels(x, y, w, h);
            Texture2D cropped = new Texture2D(w, h);
            cropped.SetPixels(sampledPixels);
            cropped.Apply();
            return cropped;
        }

        /// <summary>
        /// Sets up the camera rig for the screenshot by instantiating a specifically named object from resources.
        /// </summary>
        /// <returns>The *thumbnail* camera in the camera rig</returns>
        private static Camera Setup()
        {
            // load prefab & instantiate
            GameObject prefab = Resources.Load("Thumbnail Rig") as GameObject;
            GameObject instance = GameObject.Instantiate(prefab);
            // get the camera
            Camera camera = instance.GetComponentInChildren<Camera>();
            camera.gameObject.SetActive(false);
            return camera;
        }

        /// <summary>
        /// cleans up the camera rig
        /// </summary>
        /// <param name="camera">the thumbnail camera</param>
        private static void Teardown(Camera camera)
        {
            GameObject parent = camera.transform.parent.gameObject;
#if UNITY_EDITOR
            if (EditorApplication.isPlaying)
            {
                GameObject.Destroy(parent);
            }
            else
            {
                GameObject.DestroyImmediate(parent);
            }
#else
            GameObject.Destroy(parent);
#endif
        }

        public static void ClearQueue() {
            Debug.Log("Clear Queue");
            if(instance != null) {
                instance.StopCoroutine("DequeueThumbnail");
            }
            queue.Clear();
            isReady = false;
            isRunning = false;
            order = null;
            if(instance != null) {
                DestroyImmediate(instance);
                instance = null;
            }
        }

        public static void ClearGeneratedTextures() {
            var loaded = Resources.FindObjectsOfTypeAll<Texture2D>();
            Debug.Log("textures: " + loaded.Length);
            for(int i = 0; i < generatedTextures.Count; i++) {
                if(generatedTextures[i] != null) {
                    UnityEngine.Object.Destroy(generatedTextures[i]);
                }
            }
        }
    }

    class QueuedOrder {
        public GameObject gameObject;
        public Image image;
        public string source;
        public string name;
        public override string ToString()
        {
            return $"Thumbnail Queue Item {name}, source {source}, rendered {image != null}, time {Time.realtimeSinceStartup}, frame {Time.frameCount}";
        }
        public QueuedOrder (GameObject gameObject, Image image, string source = "unknown"){
            this.gameObject = gameObject;
            this.image = image;
            this.source = source;
            this.name = gameObject.name;
        }
    }
}