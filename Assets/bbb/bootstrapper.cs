using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BBB.View;
using UnityEngine.UI;

namespace BBB {
    public class bootstrapper : MonoBehaviour
    {
        public GameObject[] targets;
        private int index = 0;
        public Image output;
        
        // Start is called before the first frame update
        void Start()
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 30;
            Enqueue();
        }

        // Update is called once per frame
        void Update()
        {
            // Enqueue();
        }

        public void Enqueue() {
            GameObject target = targets[index];
            index++;
            index = index % targets.Length;
            Thumbnail.EnqueueThumbnail(target, output, "bootstrapper Start");
        }
    }
}
